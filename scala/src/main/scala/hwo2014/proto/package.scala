package hwo2014

import org.json4s._

package object proto {

  object MessageType {

    val Join = "join"
    val YourCar = "yourCar"
    val GameInit = "gameInit"
    val GameStart = "gameStart"
    val CarPositions = "carPositions"
    val Throttle = "throttle"
    val GameEnd = "gameEnd"
    val TournamentEnd = "tournamentEnd"
    val Crash = "crash"
    val Spawn = "spawn"
    val LapFinished = "lapFinished"
    val DNF = "dnf"
    val Finish = "finish"
    val SwitchLane = "switchLane"
    object Direction {
      val Left = "Left"
      val Right = "Right"
    }
    val Ping = "ping"
  }

  case class CarId(id: String, color: String)
  case class Join(name: String, key: String)
  case class Piece(length: Option[Double], radius: Option[Double], angle: Option[Double], switch: Boolean, bridge: Boolean)
  case class Lane(distanceFromCenter: Double, index: Int)
  case class Point(x: Double, y: Double)
  case class StartingPoint(position: Point, angle: Double)
  case class Track(id: String, name: String, pieces: Seq[Piece], lanes: Seq[Lane])
  case class CarDimensions(length: Double, width: Double, guideFlagPosition: Double)
  case class Car(id: CarId, dimensions: CarDimensions)
  case class RaceSession(laps: Int, maxLapTimeMs: Int, quickRace: Boolean)
  case class Race(track: Track, cars: Seq[Car])
  case class GameInit(race: Race)

  case class LanePosition(startLaneIndex: Int, endLaneIndex: Int)
  case class PiecePosition(pieceIndex: Int, inPieceDistance: Double, lane: LanePosition, lap: Int)
  case class CarPosition(id: CarId, angle: Double, piecePosition: PiecePosition)

  case class CarResult(laps: Int, ticks: Int, millis: Int)
  case class GameResult(car: CarId, result: CarResult)
  case class GameResults(results: Seq[GameResult], bestLaps: Seq[GameResult])

  case class MsgWrapper(msgType: String, data: JValue, gameId: Option[String], gameTick: Option[Int]) {
  }
  object MsgWrapper {
    implicit val formats = new DefaultFormats{}

    def apply(msgType: String, data: Any): MsgWrapper = {
      MsgWrapper(msgType, Extraction.decompose(data))
    }
  }




}
